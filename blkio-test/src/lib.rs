#![allow(dead_code)]

use libc::iovec;
use nix::sys::eventfd;
use nix::unistd;
use std::collections::VecDeque;
use std::os::unix::io::RawFd;
use std::sync::{Arc, Mutex};

pub struct Blkio {
    queues: Vec<Blkioq>,
}

pub struct Blkioq {
    queued: VecDeque<Request>,
    pending: Arc<Mutex<VecDeque<Request>>>,
    completed: Arc<Mutex<VecDeque<Completion>>>,

    debug_log: Arc<Mutex<VecDeque<DebugLogEntry>>>,

    eventfd: Option<RawFd>,

    thread_launched: bool,
    test_commands: Option<VecDeque<TestCommand>>,
}

#[derive(Debug)]
pub struct Error {
    desc: String,
}

pub type Result<T> = std::result::Result<T, Error>;

pub struct BlkioStartOutcome {
    pub queues: Vec<Blkioq>,
    pub poll_queues: Vec<Blkioq>,
}

struct Request {
    req: ReqParams,
    user_data: usize,
    flags: ReqFlags,
}

#[derive(Clone, Copy, Debug)]
pub struct ReqFlags {}

// `usize` can generally be used to represent pointers as integers in Rust.  This type alias only
// exists to make it explicit where we are storing a pointer as an integer, to differentiate it
// from size/length values.
type IntegerPointer = usize;

enum ReqParams {
    Read {
        start: u64,
        buf: IntegerPointer,
        len: usize,
    },

    Write {
        start: u64,
        buf: IntegerPointer,
        len: usize,
    },

    Readv {
        start: u64,
        vec: Vec<(IntegerPointer, usize)>,
        len: usize,
    },

    Writev {
        start: u64,
        vec: Vec<(IntegerPointer, usize)>,
        len: usize,
    },

    Flush,
}

pub struct Completion {
    pub user_data: usize,
    pub ret: i32,
}

pub enum TestCommand {
    Sleep { secs: f64 },
    CompleteFront { ret: i32 },
    CompleteBack { ret: i32 },
    CompleteAll { ret: i32 },
    UpdateCompletionFd,
}

#[derive(Debug)]
pub enum DebugLogEntry {
    Enqueued(DebugLogRequest),
    Pending(DebugLogRequest),
    Completed(DebugLogRequest),
    Settled { user_data: usize, ret: i32 },

    // As reported by libblkio-async itself
    Polling { user_data: usize },
}

#[derive(Debug)]
pub enum DebugLogRequest {
    Read {
        start: u64,
        len: usize,
        user_data: usize,
        flags: ReqFlags,
        vectored: bool,
    },

    Write {
        start: u64,
        len: usize,
        user_data: usize,
        flags: ReqFlags,
        vectored: bool,
    },

    Flush {
        user_data: usize,
        flags: ReqFlags,
    },
}

impl Blkio {
    pub fn new(_driver: &str) -> Result<Self> {
        Ok(Blkio { queues: Vec::new() })
    }

    pub fn connect(&mut self) -> Result<()> {
        Ok(())
    }

    pub fn start(&mut self) -> Result<BlkioStartOutcome> {
        Ok(BlkioStartOutcome {
            queues: self.queues.drain(..).collect(),
            poll_queues: Vec::new(),
        })
    }

    pub fn get_bool(&mut self, key: &str) -> Result<bool> {
        match key {
            "needs-mem-regions" => Ok(false),
            _ => Err(Error::new(format!("unrecognized option \"{}\"", key))),
        }
    }

    pub fn get_u64(&mut self, key: &str) -> Result<u64> {
        match key {
            "capacity" => Ok(1024 * 1024 * 1024),
            _ => Err(Error::new(format!("unrecognized option \"{}\"", key))),
        }
    }

    pub fn set_bool(&mut self, key: &str, _val: bool) -> Result<()> {
        match key {
            "direct" => Ok(()),
            _ => Err(Error::new(format!("unrecognized option \"{}\"", key))),
        }
    }

    pub fn set_i32(&mut self, key: &str, val: i32) -> Result<()> {
        match key {
            "num-queues" => {
                self.queues
                    .resize_with(val.try_into().unwrap(), Default::default);
                Ok(())
            }
            _ => Err(Error::new(format!("unrecognized option \"{}\"", key))),
        }
    }

    pub fn set_str(&mut self, key: &str, _val: &str) -> Result<()> {
        match key {
            "path" => Ok(()),
            _ => Err(Error::new(format!("unrecognized option \"{}\"", key))),
        }
    }

    pub fn get_queue(&mut self, index: usize) -> Result<&mut Blkioq> {
        self.queues
            .get_mut(index)
            .ok_or_else(|| Error::new(format!("invalid queue index {}", index)))
    }
}

/// Turns an iovec array into a safe representation
unsafe fn iovec_to_vec(vec: *const iovec, n: u32) -> Vec<(IntegerPointer, usize)> {
    let n: isize = n.try_into().unwrap();

    (0..n)
        .map(|i| {
            let entry: &iovec = &*vec.offset(i);
            (entry.iov_base as IntegerPointer, entry.iov_len)
        })
        .collect()
}

// It should be explicit what the starting values are, so no deriving
#[allow(clippy::derivable_impls)]
impl Default for Blkioq {
    fn default() -> Self {
        Blkioq {
            queued: Default::default(),
            pending: Default::default(),
            completed: Default::default(),

            debug_log: Default::default(),

            eventfd: None,

            thread_launched: false,
            test_commands: Some(VecDeque::new()),
        }
    }
}

impl Blkioq {
    pub fn set_completion_fd_enabled(&mut self, status: bool) {
        if status && self.eventfd.is_none() {
            self.eventfd = Some(eventfd::eventfd(0, eventfd::EfdFlags::EFD_NONBLOCK).unwrap());
        } else if !status && self.eventfd.is_some() {
            // FIXME: Close FD
            self.eventfd.take();
        }
    }

    pub fn get_completion_fd(&self) -> Option<RawFd> {
        self.eventfd
    }

    pub fn read(
        &mut self,
        start: u64,
        buf: *mut u8,
        len: usize,
        user_data: usize,
        flags: ReqFlags,
    ) {
        self.enqueue(Request {
            req: ReqParams::Read {
                start,
                buf: buf as IntegerPointer,
                len,
            },
            user_data,
            flags,
        })
    }

    pub fn write(
        &mut self,
        start: u64,
        buf: *const u8,
        len: usize,
        user_data: usize,
        flags: ReqFlags,
    ) {
        self.enqueue(Request {
            req: ReqParams::Write {
                start,
                buf: buf as IntegerPointer,
                len,
            },
            user_data,
            flags,
        })
    }

    #[allow(clippy::not_unsafe_ptr_arg_deref)]
    pub fn readv(
        &mut self,
        start: u64,
        vec: *const iovec,
        n: u32,
        user_data: usize,
        flags: ReqFlags,
    ) {
        // Caller guarantees this is safe
        let vec = unsafe { iovec_to_vec(vec, n) };
        let len = vec.iter().fold(0, |sum, element| sum + element.1);

        self.enqueue(Request {
            req: ReqParams::Readv { start, vec, len },
            user_data,
            flags,
        })
    }

    #[allow(clippy::not_unsafe_ptr_arg_deref)]
    pub fn writev(
        &mut self,
        start: u64,
        vec: *const iovec,
        n: u32,
        user_data: usize,
        flags: ReqFlags,
    ) {
        // Caller guarantees this is safe
        let vec = unsafe { iovec_to_vec(vec, n) };
        let len = vec.iter().fold(0, |sum, element| sum + element.1);

        self.enqueue(Request {
            req: ReqParams::Writev { start, vec, len },
            user_data,
            flags,
        })
    }

    pub fn flush(&mut self, user_data: usize, flags: ReqFlags) {
        self.enqueue(Request {
            req: ReqParams::Flush,
            user_data,
            flags,
        })
    }

    fn enqueue(&mut self, req: Request) {
        let dle = DebugLogEntry::Enqueued(DebugLogRequest::from(&req));
        self.debug_log.lock().unwrap().push_back(dle);

        self.queued.push_back(req);
    }

    pub fn push_test_command(&mut self, cmd: TestCommand) {
        self.test_commands.as_mut().unwrap().push_back(cmd);
    }

    pub fn next_debug_log_entry(&mut self) -> Option<DebugLogEntry> {
        self.debug_log.lock().unwrap().pop_front()
    }

    pub fn notify_poll(&mut self, user_data: usize) {
        let dle = DebugLogEntry::Polling { user_data };
        self.debug_log.lock().unwrap().push_back(dle);
    }

    pub fn do_io(
        &mut self,
        completions: &mut [std::mem::MaybeUninit<Completion>],
        min_completions: usize,
        _timeout: Option<()>,
        _sig: Option<()>,
    ) -> Result<usize> {
        assert!(min_completions == 0);

        while let Some(req) = self.queued.pop_front() {
            let dle = DebugLogEntry::Pending(DebugLogRequest::from(&req));
            self.debug_log.lock().unwrap().push_back(dle);

            self.pending.lock().unwrap().push_back(req);
        }

        if !self.thread_launched {
            let eventfd = self.eventfd.unwrap();
            let pending = Arc::clone(&self.pending);
            let completed = Arc::clone(&self.completed);
            let debug_log = Arc::clone(&self.debug_log);
            let mut test_commands = self.test_commands.take().unwrap();

            std::thread::spawn(move || {
                let completion_event: [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 42];

                while let Some(cmd) = test_commands.pop_front() {
                    match cmd {
                        TestCommand::Sleep { secs } => {
                            std::thread::sleep(std::time::Duration::from_secs_f64(secs))
                        }

                        TestCommand::CompleteFront { ret } => {
                            let req = pending.lock().unwrap().pop_front().unwrap();

                            let dle = DebugLogEntry::Completed(DebugLogRequest::from(&req));
                            debug_log.lock().unwrap().push_back(dle);

                            completed.lock().unwrap().push_back(req.completion(ret));
                        }

                        TestCommand::CompleteBack { ret } => {
                            let req = pending.lock().unwrap().pop_back().unwrap();

                            let dle = DebugLogEntry::Completed(DebugLogRequest::from(&req));
                            debug_log.lock().unwrap().push_back(dle);

                            completed.lock().unwrap().push_back(req.completion(ret));
                        }

                        TestCommand::CompleteAll { ret } => {
                            while let Some(req) = pending.lock().unwrap().pop_front() {
                                let dle = DebugLogEntry::Completed(DebugLogRequest::from(&req));
                                debug_log.lock().unwrap().push_back(dle);

                                completed.lock().unwrap().push_back(req.completion(ret));
                            }
                        }

                        TestCommand::UpdateCompletionFd => {
                            if completed.lock().unwrap().len() > 0 {
                                unistd::write(eventfd, &completion_event).unwrap();
                            }
                        }
                    }
                }

                // After all test commands are done, keep completing requests
                loop {
                    std::thread::sleep(std::time::Duration::from_millis(10));

                    let mut completed_something = false;
                    while let Some(req) = pending.lock().unwrap().pop_front() {
                        let dle = DebugLogEntry::Completed(DebugLogRequest::from(&req));
                        debug_log.lock().unwrap().push_back(dle);

                        completed.lock().unwrap().push_back(req.completion(0));
                        completed_something = true;
                    }
                    if completed_something {
                        unistd::write(eventfd, &completion_event).unwrap();
                    }
                }
            });

            self.thread_launched = true;
        }

        let completion_count = {
            let mut completed = self.completed.lock().unwrap();
            if completed.is_empty() {
                0
            } else {
                let count = std::cmp::min(completions.len(), completed.len());
                for completion_buf in completions.iter_mut().take(count) {
                    let creq = completed.pop_front().unwrap();

                    let dle = DebugLogEntry::Settled {
                        user_data: creq.user_data,
                        ret: creq.ret,
                    };
                    self.debug_log.lock().unwrap().push_back(dle);

                    completion_buf.write(creq);
                }
                count
            }
        };

        Ok(completion_count)
    }
}

impl Error {
    fn new(desc: String) -> Self {
        Error { desc }
    }

    pub fn message(&self) -> &str {
        &self.desc
    }
}

impl Request {
    fn completion(self, ret: i32) -> Completion {
        Completion {
            user_data: self.user_data,
            ret,
        }
    }
}

impl DebugLogRequest {
    fn from(req: &Request) -> Self {
        match req.req {
            ReqParams::Read { start, buf: _, len } => DebugLogRequest::Read {
                start,
                len,
                user_data: req.user_data,
                flags: req.flags,
                vectored: false,
            },

            ReqParams::Write { start, buf: _, len } => DebugLogRequest::Write {
                start,
                len,
                user_data: req.user_data,
                flags: req.flags,
                vectored: false,
            },

            ReqParams::Readv { start, vec: _, len } => DebugLogRequest::Read {
                start,
                len,
                user_data: req.user_data,
                flags: req.flags,
                vectored: true,
            },

            ReqParams::Writev { start, vec: _, len } => DebugLogRequest::Write {
                start,
                len,
                user_data: req.user_data,
                flags: req.flags,
                vectored: true,
            },

            ReqParams::Flush => DebugLogRequest::Flush {
                user_data: req.user_data,
                flags: req.flags,
            },
        }
    }
}

impl ReqFlags {
    pub fn empty() -> Self {
        ReqFlags {}
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.desc)
    }
}

impl std::error::Error for Error {}
