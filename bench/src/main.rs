mod pinvec;

use crate::pinvec::PinVec;
#[cfg(not(feature = "x-blkio-test"))]
use blkio::{Blkio, Blkioq, ReqFlags};
use blkio_async::AsyncBlkioq;
use clap::Parser;
#[cfg(all(feature = "x-blkio-test", test))]
use std::collections::HashMap;
use std::future::Future;
use std::io::{self, Write};
use std::pin::Pin;
use std::task::{Context, Poll};

#[cfg(feature = "x-blkio-test")]
use blkio_test as blkio;
#[cfg(feature = "x-blkio-test")]
use blkio_test::{Blkio, Blkioq, ReqFlags};
#[cfg(all(feature = "x-blkio-test", test))]
use blkio_test::{DebugLogEntry, DebugLogRequest, TestCommand};


#[cfg(all(feature = "x-blkio-test", not(test)))]
compile_error!("The x-blkio-test feature must only be used for testing");

#[cfg(all(not(feature = "x-blkio-test"), test))]
compile_error!("Testing requires enabling the x-blkio-test feature");


fn blkio_err_to_io(err: blkio::Error) -> io::Error {
    io::Error::new(io::ErrorKind::Other, err.message())
}

fn setup_test_file(path: &str, length: usize) -> io::Result<()> {
    let mut file = std::fs::OpenOptions::new()
        .create(true)
        .truncate(true)
        .write(true)
        .open(path)?;

    let buf = [42u8; 65536];
    let mut remaining = length;
    while remaining > 0 {
        let to_write = if remaining >= 65536 { 65536 } else { remaining };
        file.write_all(&buf[0..to_write])?;
        remaining -= to_write;
    }

    Ok(())
}

// The returned reference to the `Blkioq` is owned by the `Blkio` object, and so its lifetime is
// limited accordingly.  However, first, that is impossible to express, and second, the
// `AsyncBlkioq` constructor expects a static-lifetime reference anyway, so return such a
// reference.
fn setup_device(path: &str, direct: bool) -> Result<(Blkio, Blkioq, u64), blkio::Error> {
    let mut blkio = Blkio::new("io_uring")?;
    blkio.set_str("path", path)?;
    blkio.set_bool("direct", direct)?;
    blkio.connect()?;

    assert!(!(blkio.get_bool("needs-mem-regions")?));
    blkio.set_i32("num-queues", 1)?;
    let start_outcome = blkio.start()?;
    let blkioq = start_outcome.queues.into_iter().next().unwrap();

    let flen = blkio.get_u64("capacity")?;

    Ok((blkio, blkioq, flen))
}

async fn run_single(
    blkioq: &mut AsyncBlkioq,
    flen: u64,
    total: usize,
) -> io::Result<std::time::Duration> {
    let request_size = 4096;
    let alignment = 4096;

    let mut buf = vec![0u8; request_size + alignment - 1];

    let mut head = alignment - (buf.as_ptr() as usize) % alignment;
    if head == alignment {
        head = 0;
    }

    let aligned_buffer = unsafe {
        std::mem::transmute::<&'_ mut [u8], &'static mut [u8]>(
            &mut buf[head..(head + request_size)],
        )
    };

    let mut ofs = 0;
    let max_ofs = flen - request_size as u64;

    let start = std::time::Instant::now();
    for i in 0..total {
        if let Err(e) = blkioq.read(ofs, aligned_buffer, ReqFlags::empty()).await {
            eprintln!("{}: {}", i, e);
        }

        ofs += request_size as u64;
        if ofs > max_ofs {
            ofs = 0;
        }
    }
    let end = std::time::Instant::now();

    Ok(end - start)
}

struct RequestSlot<'a> {
    future: Option<blkio_async::Request<'a>>,

    #[allow(dead_code)]
    backing_buffer: Vec<u8>,
    aligned_buffer: &'a mut [u8],
}

struct ParRunner<'a> {
    blkioq: &'a mut AsyncBlkioq,
    remaining_to_submit: usize,
    remaining_to_settle: usize,
    simultaneous: usize,

    poll_index: usize,

    ofs: u64,
    max_ofs: u64,

    reqs: PinVec<RequestSlot<'a>>,
}

struct ParDrainRunner<'a> {
    blkioq: &'a mut AsyncBlkioq,
    remaining_to_submit: usize,
    remaining_to_settle: usize,
    simultaneous: usize,

    ofs: u64,
    max_ofs: u64,

    reqs: PinVec<RequestSlot<'a>>,
}

impl<'a> RequestSlot<'a> {
    fn new(length: usize, alignment: usize) -> Self {
        let mut buf = vec![0u8; length + alignment - 1];

        let mut head = alignment - (buf.as_ptr() as usize) % alignment;
        if head == alignment {
            head = 0;
        }

        let aligned_buffer = unsafe {
            std::mem::transmute::<&'_ mut [u8], &'static mut [u8]>(&mut buf[head..(head + length)])
        };

        RequestSlot {
            future: None,

            backing_buffer: buf,
            aligned_buffer,
        }
    }

    /// This function exists merely to trick the borrow checker
    fn get_buffer(&mut self) -> &'a mut [u8] {
        unsafe { &mut *(self.aligned_buffer as *mut [u8]) }
    }
}

impl<'a> ParRunner<'a> {
    fn new(
        blkioq: &'a mut AsyncBlkioq,
        simultaneous: usize,
        total: usize,
        file_length: u64,
    ) -> Self {
        ParRunner {
            blkioq,
            remaining_to_submit: total,
            remaining_to_settle: total,
            simultaneous,

            poll_index: 0,

            ofs: 0,
            max_ofs: file_length - 4096,

            reqs: (0..simultaneous)
                .map(|_| RequestSlot::new(4096, 4096))
                .collect::<Vec<RequestSlot<'a>>>()
                .into(),
        }
    }
}

impl<'a> ParDrainRunner<'a> {
    fn new(
        blkioq: &'a mut AsyncBlkioq,
        simultaneous: usize,
        total: usize,
        file_length: u64,
    ) -> Self {
        ParDrainRunner {
            blkioq,
            remaining_to_submit: total,
            remaining_to_settle: total,
            simultaneous,

            ofs: 0,
            max_ofs: file_length - 4096,

            reqs: (0..simultaneous)
                .map(|_| RequestSlot::new(4096, 4096))
                .collect::<Vec<RequestSlot<'a>>>()
                .into(),
        }
    }
}

impl Future for ParRunner<'_> {
    type Output = io::Result<()>;

    fn poll(mut self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<io::Result<()>> {
        let mut continuously_pending = 0;

        while continuously_pending < self.simultaneous && self.remaining_to_settle > 0 {
            let i = self.poll_index;
            self.poll_index = (self.poll_index + 1) % self.simultaneous;
            continuously_pending += 1;

            let mut req = self.reqs.get_mut(i);
            if let Some(fut) = req.future.as_mut() {
                match Future::poll(Pin::new(fut), ctx) {
                    Poll::Pending => continue,
                    Poll::Ready(Err(e)) => return Poll::Ready(Err(e)),
                    Poll::Ready(Ok(())) => (),
                };
                req.future.take();

                self.remaining_to_settle -= 1;
            }

            if self.remaining_to_submit == 0 {
                continue;
            }

            let buf = self.reqs.get_mut(i).get_buffer();
            let fut = self.blkioq.read(self.ofs, buf, ReqFlags::empty());
            self.reqs.get_mut(i).future.replace(fut);

            self.remaining_to_submit -= 1;
            self.ofs += 4096;
            if self.ofs > self.max_ofs {
                self.ofs = 0;
            }

            continuously_pending = 0;
        }

        if self.remaining_to_settle == 0 {
            Poll::Ready(Ok(()))
        } else {
            Poll::Pending
        }
    }
}

impl Future for ParDrainRunner<'_> {
    type Output = io::Result<()>;

    fn poll(mut self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<io::Result<()>> {
        let mut pending = 0;

        // We can only return pending if `ctx`'s is registered somewhere, so keep polling until at
        // least one request returns `Poll::Pending`
        while pending == 0 {
            for i in 0..self.simultaneous {
                let mut req = self.reqs.get_mut(i);
                if let Some(fut) = req.future.as_mut() {
                    match Future::poll(Pin::new(fut), ctx) {
                        Poll::Pending => {
                            pending += 1;
                        }
                        Poll::Ready(Err(e)) => return Poll::Ready(Err(e)),
                        Poll::Ready(Ok(())) => {
                            req.future.take();
                            self.remaining_to_settle -= 1;
                        }
                    }
                }
            }

            if pending == 0 {
                if self.remaining_to_settle == 0 {
                    return Poll::Ready(Ok(()));
                }

                for i in 0..std::cmp::min(self.simultaneous, self.remaining_to_submit) {
                    let buf = self.reqs.get_mut(i).get_buffer();
                    let fut = self.blkioq.read(self.ofs, buf, ReqFlags::empty());
                    self.reqs.get_mut(i).future.replace(fut);

                    self.remaining_to_submit -= 1;
                    self.ofs += 4096;
                    if self.ofs > self.max_ofs {
                        self.ofs = 0;
                    }
                }
            }
        }

        Poll::Pending
    }
}

async fn run_parallel(
    blkioq: &mut AsyncBlkioq,
    flen: u64,
    total: usize,
    par: usize,
    drain_batch: bool,
) -> io::Result<std::time::Duration> {
    let runner: Pin<Box<dyn Future<Output = io::Result<()>>>> = if drain_batch {
        Box::pin(ParDrainRunner::new(blkioq, par, total, flen))
    } else {
        Box::pin(ParRunner::new(blkioq, par, total, flen))
    };

    let start = std::time::Instant::now();
    runner.await?;
    let end = std::time::Instant::now();

    Ok(end - start)
}

#[derive(clap::Parser)]
struct CommandLineOpts {
    #[clap(long)]
    /// Create the test file before the test run
    create: bool,

    #[clap(long, default_value = "/tmp/test")]
    /// Path to the test file
    path: String,

    #[clap(long)]
    /// Use `O_DIRECT`
    direct: bool,

    #[clap(long, default_value = "1048576")]
    /// Total number of requests to perform
    total: usize,

    #[clap(subcommand)]
    command: Command,
}

#[derive(clap::Parser)]
enum Command {
    /// Perform only a single request at a time
    Single,

    /// Perform multiple requests in parallel
    Parallel {
        #[clap(long, default_value = "8")]
        /// Number of requests to issue simultaneously
        simultaneous: usize,

        #[clap(long)]
        /// Always wait for all simultaneously running requests to settle before issuing a new batch
        drain_batch: bool,
    },
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> io::Result<()> {
    let opt = CommandLineOpts::parse();

    if opt.create {
        setup_test_file(&opt.path, 4096)?;
    }

    let (_device, blkioq, flen) = setup_device(&opt.path, opt.direct).map_err(blkio_err_to_io)?;
    let mut blkioq = AsyncBlkioq::new(blkioq);

    let duration = match opt.command {
        Command::Single => run_single(&mut blkioq, flen, opt.total).await?,
        Command::Parallel {
            simultaneous,
            drain_batch,
        } => run_parallel(&mut blkioq, flen, opt.total, simultaneous, drain_batch).await?,
    };

    println!(
        "{} ops in {:?} = {} IOPS",
        opt.total,
        duration,
        opt.total as f64 / duration.as_secs_f64()
    );

    Ok(())
}

/// Testing area

/**
 * The following five functions:
 * - test_verify_enqueued()
 * - test_verify_pending()
 * - test_verify_completed()
 * - test_verify_settled()
 * - test_verify_polling()
 * each verify that a range of requests is in the blkioq debug log in the expected stage.
 */

#[cfg(all(feature = "x-blkio-test", test))]
fn test_verify_enqueued<I: Iterator<Item = u64>>(
    blkioq: &mut AsyncBlkioq,
    user_data_map: &mut HashMap<u64, usize>,
    range: I,
    start_ofs: u64,
) {
    for i in range {
        match blkioq.next_debug_log_entry() {
            Some(DebugLogEntry::Enqueued(DebugLogRequest::Read {
                start,
                len: 4096,
                user_data,
                flags: _,
                vectored: false,
            })) => {
                assert!(
                    start == start_ofs + i * 4096,
                    "Expected {} as start, got {}",
                    start_ofs + i * 4096,
                    start
                );
                let x = user_data_map.insert(i, user_data);
                assert!(x.is_none());
            }

            x => panic!(
                "Expecting: {:?}\nGot: {:?}",
                Some(DebugLogEntry::Enqueued(DebugLogRequest::Read {
                    start: start_ofs + i * 4096,
                    len: 4096,
                    user_data: 0,
                    flags: ReqFlags::empty(),
                    vectored: false,
                })),
                x
            ),
        }
    }
}

#[cfg(all(feature = "x-blkio-test", test))]
fn test_verify_pending<I: Iterator<Item = u64>>(
    blkioq: &mut AsyncBlkioq,
    user_data_map: &HashMap<u64, usize>,
    range: I,
) {
    for i in range {
        match blkioq.next_debug_log_entry() {
            Some(DebugLogEntry::Pending(DebugLogRequest::Read {
                start: _,
                len: 4096,
                user_data,
                flags: _,
                vectored: false,
            })) => assert!(
                user_data == *user_data_map.get(&i).unwrap(),
                "Expected {} ({}) as user data, got {}",
                user_data_map.get(&i).unwrap(),
                i,
                user_data,
            ),

            x => panic!(
                "Expecting: {:?}\nGot: {:?}",
                Some(DebugLogEntry::Pending(DebugLogRequest::Read {
                    start: 0,
                    len: 4096,
                    user_data: *user_data_map.get(&i).unwrap(),
                    flags: ReqFlags::empty(),
                    vectored: false,
                })),
                x
            ),
        }
    }
}

#[cfg(all(feature = "x-blkio-test", test))]
fn test_verify_completed<I: Iterator<Item = u64>>(
    blkioq: &mut AsyncBlkioq,
    user_data_map: &HashMap<u64, usize>,
    range: I,
) {
    for i in range {
        match blkioq.next_debug_log_entry() {
            Some(DebugLogEntry::Completed(DebugLogRequest::Read {
                start: _,
                len: 4096,
                user_data,
                flags: _,
                vectored: false,
            })) => assert!(
                user_data == *user_data_map.get(&i).unwrap(),
                "Expected {} ({}) as user data, got {}",
                user_data_map.get(&i).unwrap(),
                i,
                user_data,
            ),

            x => panic!(
                "Expecting: {:?}\nGot: {:?}",
                Some(DebugLogEntry::Completed(DebugLogRequest::Read {
                    start: 0,
                    len: 4096,
                    user_data: *user_data_map.get(&i).unwrap(),
                    flags: ReqFlags::empty(),
                    vectored: false,
                })),
                x
            ),
        }
    }
}

#[cfg(all(feature = "x-blkio-test", test))]
fn test_verify_settled<I: Iterator<Item = u64>>(
    blkioq: &mut AsyncBlkioq,
    user_data_map: &HashMap<u64, usize>,
    range: I,
) {
    for i in range {
        match blkioq.next_debug_log_entry() {
            Some(DebugLogEntry::Settled { user_data, ret: 0 }) => assert!(
                user_data == *user_data_map.get(&i).unwrap(),
                "Expected {} ({}) as user data, got {}",
                user_data_map.get(&i).unwrap(),
                i,
                user_data,
            ),

            x => panic!(
                "Expecting: {:?}\nGot: {:?}",
                Some(DebugLogEntry::Settled {
                    user_data: *user_data_map.get(&i).unwrap(),
                    ret: 0,
                }),
                x
            ),
        }
    }
}

#[cfg(all(feature = "x-blkio-test", test))]
fn test_verify_polling<I: Iterator<Item = u64>>(
    blkioq: &mut AsyncBlkioq,
    user_data_map: &HashMap<u64, usize>,
    range: I,
) {
    for i in range {
        match blkioq.next_debug_log_entry() {
            Some(DebugLogEntry::Polling { user_data }) => assert!(
                user_data == *user_data_map.get(&i).unwrap(),
                "Expected {} ({}) as user data, got {}",
                user_data_map.get(&i).unwrap(),
                i,
                user_data,
            ),

            x => panic!(
                "Expecting: {:?}\nGot: {:?}",
                Some(DebugLogEntry::Polling {
                    user_data: *user_data_map.get(&i).unwrap(),
                }),
                x
            ),
        }
    }
}

/**
 * Complete the first and last request in the queue.  See that this neither hangs nor results in a
 * never-pausing polling loop.
 *
 * As for _polling() checks, note that tokio always polls all requests in a context once the
 * context is woken.  The poll() function invokes do_io(), which is why Enqueued => Pending and
 * Completed => Settled state changes always happen after a single poll().
 */
#[tokio::test]
#[cfg(feature = "x-blkio-test")]
async fn test_0() -> io::Result<()> {
    let (_device, mut blkioq, flen) = setup_device("", false).map_err(blkio_err_to_io)?;

    blkioq.push_test_command(TestCommand::Sleep { secs: 0.1 });
    blkioq.push_test_command(TestCommand::CompleteFront { ret: 0 });
    blkioq.push_test_command(TestCommand::CompleteBack { ret: 0 });
    blkioq.push_test_command(TestCommand::UpdateCompletionFd);
    blkioq.push_test_command(TestCommand::Sleep { secs: 0.1 });

    let mut blkioq = AsyncBlkioq::new(blkioq);

    run_parallel(&mut blkioq, flen, 8, 4, true).await?;

    let mut user_data = HashMap::<u64, usize>::new();

    // All 4 simultaneous requests enqueued and then pending (after the first poll)
    test_verify_enqueued(&mut blkioq, &mut user_data, 0..=3, 0);
    test_verify_polling(&mut blkioq, &user_data, 0..=0);
    test_verify_pending(&mut blkioq, &user_data, 0..=3);
    test_verify_polling(&mut blkioq, &user_data, 1..=3);

    // Front and back requests completed (prompting a poll) and settled (once polled)
    test_verify_completed(&mut blkioq, &user_data, (0..=3).step_by(3));
    test_verify_polling(&mut blkioq, &user_data, 0..=0);
    test_verify_settled(&mut blkioq, &user_data, (0..=3).step_by(3));
    test_verify_polling(&mut blkioq, &user_data, 1..=3);

    // Re-poll, polling only the remaining requests
    test_verify_polling(&mut blkioq, &user_data, 1..=2);

    // Remaining two requests (middle) completed (prompting a poll) and settled (once polled)
    test_verify_completed(&mut blkioq, &user_data, 1..=2);
    test_verify_polling(&mut blkioq, &user_data, 1..=1);
    test_verify_settled(&mut blkioq, &user_data, 1..=2);
    test_verify_polling(&mut blkioq, &user_data, 2..=2);

    // Remaining 4 requests enqueued, pending, completed, settled
    test_verify_enqueued(&mut blkioq, &mut user_data, 4..=7, 0);
    test_verify_polling(&mut blkioq, &user_data, 4..=4);
    test_verify_pending(&mut blkioq, &user_data, 4..=7);
    test_verify_polling(&mut blkioq, &user_data, 5..=7);

    test_verify_completed(&mut blkioq, &user_data, 4..=7);
    test_verify_polling(&mut blkioq, &user_data, 4..=4);
    test_verify_settled(&mut blkioq, &user_data, 4..=7);
    test_verify_polling(&mut blkioq, &user_data, 5..=7);

    assert!(blkioq.next_debug_log_entry().is_none());

    Ok(())
}

/**
 * Same as test_0(), but with drain_batch == false.
 */
#[tokio::test]
#[cfg(feature = "x-blkio-test")]
async fn test_1() -> io::Result<()> {
    let (_device, mut blkioq, flen) = setup_device("", false).map_err(blkio_err_to_io)?;

    blkioq.push_test_command(TestCommand::Sleep { secs: 0.1 });
    blkioq.push_test_command(TestCommand::CompleteFront { ret: 0 });
    blkioq.push_test_command(TestCommand::CompleteBack { ret: 0 });
    blkioq.push_test_command(TestCommand::UpdateCompletionFd);
    blkioq.push_test_command(TestCommand::Sleep { secs: 0.1 });

    let mut blkioq = AsyncBlkioq::new(blkioq);

    run_parallel(&mut blkioq, flen, 8, 4, false).await?;

    let mut user_data = HashMap::<u64, usize>::new();

    // All 4 simultaneous requests enqueued and then pending (after the first poll)
    test_verify_enqueued(&mut blkioq, &mut user_data, 0..=3, 0);
    test_verify_polling(&mut blkioq, &user_data, 0..=0);
    test_verify_pending(&mut blkioq, &user_data, 0..=3);
    test_verify_polling(&mut blkioq, &user_data, 1..=3);

    // Front and back requests completed (prompting a poll) and settled (once polled).
    // New requests are enqueued immediately, and get pending after a poll.
    test_verify_completed(&mut blkioq, &user_data, (0..=3).step_by(3));
    test_verify_polling(&mut blkioq, &user_data, 0..=0);
    test_verify_settled(&mut blkioq, &user_data, (0..=3).step_by(3));

    // Req 4 is enqueued and pending
    test_verify_enqueued(&mut blkioq, &mut user_data, 4..=4, 0);
    test_verify_polling(&mut blkioq, &user_data, 1..=1);
    test_verify_pending(&mut blkioq, &user_data, 4..=4);
    test_verify_polling(&mut blkioq, &user_data, 2..=3);

    // Req 5 is enqueued and pending
    test_verify_enqueued(&mut blkioq, &mut user_data, 5..=5, 0);

    // Mind the quite strange polling order (4 => 1 => 2 => 5)
    test_verify_polling(&mut blkioq, &user_data, 4..=4);
    test_verify_pending(&mut blkioq, &user_data, 5..=5);
    test_verify_polling(&mut blkioq, &user_data, 1..=2);
    test_verify_polling(&mut blkioq, &user_data, 5..=5);

    // Re-poll
    test_verify_polling(&mut blkioq, &user_data, 4..=4);
    test_verify_polling(&mut blkioq, &user_data, 1..=2);
    test_verify_polling(&mut blkioq, &user_data, 5..=5);

    // All four currently pending requests completed and settled
    test_verify_completed(&mut blkioq, &user_data, 1..=2);
    test_verify_completed(&mut blkioq, &user_data, 4..=5);

    test_verify_polling(&mut blkioq, &user_data, 4..=4);
    test_verify_settled(&mut blkioq, &user_data, 1..=2);
    test_verify_settled(&mut blkioq, &user_data, 4..=5);

    // Remaining two requests are enqueued (interrupted by polling of the outgoing requests)
    test_verify_enqueued(&mut blkioq, &mut user_data, 6..=6, 0);
    test_verify_polling(&mut blkioq, &user_data, 1..=1);
    test_verify_enqueued(&mut blkioq, &mut user_data, 7..=7, 0);
    test_verify_polling(&mut blkioq, &user_data, 2..=2);
    test_verify_polling(&mut blkioq, &user_data, 5..=5);

    // Now 6 and 7 are pending and the rest is gone; 6 and 7 are just polled, completed, and
    // settled without anything special happening
    test_verify_polling(&mut blkioq, &user_data, 6..=6);
    test_verify_pending(&mut blkioq, &user_data, 6..=7);
    test_verify_polling(&mut blkioq, &user_data, 7..=7);

    test_verify_completed(&mut blkioq, &user_data, 6..=7);
    test_verify_polling(&mut blkioq, &user_data, 6..=6);
    test_verify_settled(&mut blkioq, &user_data, 6..=7);
    test_verify_polling(&mut blkioq, &user_data, 7..=7);

    assert!(blkioq.next_debug_log_entry().is_none());

    Ok(())
}
