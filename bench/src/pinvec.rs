use std::pin::Pin;

pub struct PinVec<T> {
    storage: Vec<T>,
}

impl<T> PinVec<T> {
    pub fn get_mut(&mut self, index: usize) -> Pin<&mut T> {
        unsafe { Pin::new_unchecked(&mut self.storage[index]) }
    }
}

impl<T> From<Vec<T>> for PinVec<T> {
    fn from(v: Vec<T>) -> PinVec<T> {
        PinVec { storage: v }
    }
}
