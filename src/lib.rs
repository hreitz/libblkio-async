use std::cell::{Cell, RefCell};
use std::future::Future;
use std::io::{self, IoSlice, IoSliceMut};
use std::os::unix::io::RawFd;
use std::pin::Pin;
use std::rc::Rc;
use std::task::{Context, Poll};
use tokio::io::unix::AsyncFd;

#[cfg(feature = "x-blkio-test")]
use blkio_test as blkio;

/**
 * Encapsulates the blkio queue and other global state.  Cannot be shared between threads, and does
 * not support multiple contexts, which means that moving a queue to a different task
 * (`std::task::Context`) requires that no requests are in flight.
 */
pub struct Queue {
    blkioq: RefCell<blkio::Blkioq>,

    /// Object to poll the completion FD.  When a request wants to poll this for readability, it
    /// needs to take ownership of the object and steal it from here, returning it once the request
    /// is polled again.  This is so that only one request awaits readability at a time (they all
    /// share the same context).
    completion_fd: Cell<Option<AsyncFd<RawFd>>>,

    /// Raw completion FD, so that all requests can read from it when we get completions,
    /// regardless of `completion_fd` is present or not.
    raw_completion_fd: RawFd,

    /// Number of in-flight not-yet-completed requests
    in_flight: Cell<usize>,

    /// On first poll, store the waker here; on subsequent polls, compare it against future wakers
    /// to verify they are all the same (a `Queue` cannot be shared between contexts).
    #[cfg(debug_assertions)]
    compare_waker: Cell<Option<std::task::Waker>>,
}

/**
 * Represents a blkio queue with an async interface.  Does not implement `Send` or `Sync` and so
 * can only be used by a single thread at a time, and must be used by that thread.  Also, it can
 * only be used by a single context at a time, so when moving it between tasks
 * (`std::task::Context`), no requests must be in flight.
 */
pub struct AsyncBlkioq {
    queue: Rc<Queue>,
}

/**
 * Core state of an ongoing request.  This object is separate from `Request` so that it can be put
 * in an `Rc` and be referenced from multiple sources, namely:
 * - `Request.rs`
 * - The user data field of requests submitted to libblkio
 *
 * Note: Putting `RequestState` into an `Rc` incurs a heap allocation and deallocation for every
 * request.  Merging `RequestState` into `Request` in order to prevent that however has only led to
 * a degredation in performance in practice (issue #1), presumably because the `Request` object is
 * moved in memory before being polled, and so an increase in size of the `Request` object is worse
 * than doing one allocation/deallocation.
 */
struct RequestState {
    /// Positive: In progress
    /// Zero: Completed successfully
    /// Negative: Failed
    ret: Cell<i32>,

    /// If this request is awaiting the completion FD to become readable, it steals ownership from
    /// `Queue` and puts the `AsyncFd` here.
    completion_fd: Cell<Option<AsyncFd<RawFd>>>,

    queue: Rc<Queue>,

    /// Optional: I/O vector whose pointer has been passed to libblkio to execute the request.
    /// Putting it here will ensure that it exists until the request is done.
    owned_iovec: Option<Box<[libc::iovec]>>,
}

/**
 * Represents an async I/O request, and so implements the `Future` trait.  Like `AsyncBlkioq`, this
 * struct does not implement `Send` or `Sync`.
 */
pub struct Request<'a> {
    /// The actual request state.  From benchmarking, it appears that putting as much data as
    /// possible there is best for performance: We need a heap allocation for it anyway, so its
    /// size does not matter.  In contrast, it appears that Rust moves `Future` objects after their
    /// creation before the first poll (by which point they must be pinned), and so the bigger the
    /// object is, the more data has to be moved.
    /// (If all of `RequestState` were to be merged into `Request`, it would be so much data that
    /// copying/moving it takes more time than the heap allocation/deallocation (see also the note
    /// above `RequestState`).)
    rs: Rc<RequestState>,

    _lifetime: std::marker::PhantomData<&'a mut [u8]>,
}

impl AsyncBlkioq {
    /**
     * Create an `AsyncBlkioq` encapsulating a blkio queue.
     */
    pub fn new(blkioq: blkio::Blkioq) -> Self {
        AsyncBlkioq {
            queue: Rc::new(Queue::new(blkioq)),
        }
    }

    /**
     * Create a new request object, and return it and a strong reference (pointer) to the
     * associated `RequestState`, which can be turned into an integer and used as a blkio request's
     * user data.
     *
     * Callers may pass an `owned_iovec` to be put into the request object, ensuring that it
     * remains allocated for the duration of the request.
     */
    fn new_request<'a>(
        &self,
        owned_iovec: Option<Box<[libc::iovec]>>,
    ) -> (Request<'a>, *const RequestState) {
        let rs = Rc::new(RequestState::new(&self.queue, owned_iovec));
        let rs_ptr = Rc::into_raw(Rc::clone(&rs));

        let r = Request {
            rs,
            _lifetime: std::marker::PhantomData,
        };

        self.queue.in_flight.set(self.queue.in_flight.get() + 1);

        (r, rs_ptr)
    }

    /**
     * Issue a read request.
     */
    pub fn read<'a>(&self, start: u64, buf: &'a mut [u8], flags: blkio::ReqFlags) -> Request<'a> {
        let (r, rs_ptr) = self.new_request(None);

        self.queue.blkioq.borrow_mut().read(
            start,
            buf.as_mut_ptr(),
            buf.len(),
            rs_ptr as usize,
            flags,
        );

        r
    }

    /**
     * Issue a read request, passing a reference to a slice of buffers.
     */
    pub fn readv<'a>(
        &self,
        start: u64,
        buf_vec: &'a mut [IoSliceMut<'a>],
        flags: blkio::ReqFlags,
    ) -> Request<'a> {
        let (r, rs_ptr) = self.new_request(None);

        // IoSliceMut and libc::iovec are guaranteed to have the same representation
        let iovec = unsafe {
            std::mem::transmute::<*const IoSliceMut, *const libc::iovec>(buf_vec.as_ptr())
        };

        self.queue.blkioq.borrow_mut().readv(
            start,
            iovec,
            buf_vec.len() as u32,
            rs_ptr as usize,
            flags,
        );

        r
    }

    /**
     * Issue a read request, passing ownership to a vector of buffers.
     */
    pub fn readv_owned<'a>(
        &self,
        start: u64,
        buf_vec: Vec<IoSliceMut<'a>>,
        flags: blkio::ReqFlags,
    ) -> Request<'a> {
        let iovec_cnt = buf_vec.len();
        let buf_vec = buf_vec.into_boxed_slice();

        // IoSliceMut and libc::iovec are guaranteed to have the same representation
        let iovec =
            unsafe { std::mem::transmute::<Box<[IoSliceMut]>, Box<[libc::iovec]>>(buf_vec) };

        let (r, rs_ptr) = self.new_request(Some(iovec));

        self.queue.blkioq.borrow_mut().readv(
            start,
            r.rs.owned_iovec.as_ref().unwrap().as_ptr(),
            iovec_cnt as u32,
            rs_ptr as usize,
            flags,
        );

        r
    }

    /**
     * Issue a write request.
     */
    pub fn write<'a>(&self, start: u64, buf: &'a [u8], flags: blkio::ReqFlags) -> Request<'a> {
        let (r, rs_ptr) = self.new_request(None);

        self.queue.blkioq.borrow_mut().write(
            start,
            buf.as_ptr(),
            buf.len(),
            rs_ptr as usize,
            flags,
        );

        r
    }

    /**
     * Issue a write request, passing a reference to a slice of buffers.
     */
    pub fn writev<'a>(
        &self,
        start: u64,
        buf_vec: &'a [IoSlice<'a>],
        flags: blkio::ReqFlags,
    ) -> Request<'a> {
        let (r, rs_ptr) = self.new_request(None);

        // IoSlice and libc::iovec are guaranteed to have the same representation
        let iovec =
            unsafe { std::mem::transmute::<*const IoSlice, *const libc::iovec>(buf_vec.as_ptr()) };

        self.queue.blkioq.borrow_mut().writev(
            start,
            iovec,
            buf_vec.len() as u32,
            rs_ptr as usize,
            flags,
        );

        r
    }

    /**
     * Issue a write request, passing ownership to a vector of buffers.
     */
    pub fn writev_owned<'a>(
        &self,
        start: u64,
        buf_vec: Vec<IoSlice<'a>>,
        flags: blkio::ReqFlags,
    ) -> Request<'a> {
        let iovec_cnt = buf_vec.len();
        let buf_vec = buf_vec.into_boxed_slice();

        // IoSlice and libc::iovec are guaranteed to have the same representation
        let iovec = unsafe { std::mem::transmute::<Box<[IoSlice]>, Box<[libc::iovec]>>(buf_vec) };

        let (r, rs_ptr) = self.new_request(Some(iovec));

        self.queue.blkioq.borrow_mut().writev(
            start,
            r.rs.owned_iovec.as_ref().unwrap().as_ptr(),
            iovec_cnt as u32,
            rs_ptr as usize,
            flags,
        );

        r
    }

    /**
     * Issue a flush.
     */
    pub fn flush<'a>(&self, flags: blkio::ReqFlags) -> Request<'a> {
        let (r, rs_ptr) = self.new_request(None);

        self.queue.blkioq.borrow_mut().flush(rs_ptr as usize, flags);

        r
    }

    #[cfg(feature = "x-blkio-test")]
    pub fn push_test_command(&self, cmd: blkio::TestCommand) {
        self.queue.blkioq.borrow_mut().push_test_command(cmd)
    }

    #[cfg(feature = "x-blkio-test")]
    pub fn next_debug_log_entry(&self) -> Option<blkio::DebugLogEntry> {
        self.queue.blkioq.borrow_mut().next_debug_log_entry()
    }
}

impl Queue {
    /**
     * Create a new `Queue` object, encapsulating the given blkio queue.
     */
    fn new(mut blkioq: blkio::Blkioq) -> Self {
        blkioq.set_completion_fd_enabled(true);
        let cfd = blkioq.get_completion_fd().unwrap();

        Queue {
            blkioq: RefCell::new(blkioq),
            completion_fd: Cell::new(Some(AsyncFd::new(cfd).unwrap())),
            raw_completion_fd: cfd,
            in_flight: Cell::new(0),
            #[cfg(debug_assertions)]
            compare_waker: Cell::new(None),
        }
    }

    /**
     * Fetch completions from libblkio.  If there are any, wake the context.
     */
    fn poll_completions(&self, cx: &mut Context<'_>) -> io::Result<()> {
        // Safe because the array elements are still MaybeUninit<_>
        let mut completions: [std::mem::MaybeUninit<blkio::Completion>; 64] =
            unsafe { std::mem::MaybeUninit::uninit().assume_init() };

        let mut completed_count = 0;

        loop {
            let completed = match self
                .blkioq
                .borrow_mut()
                .do_io(&mut completions, 0, None, None)
            {
                Ok(0) => break,
                Ok(c) => c,
                Err(err) => return Err(io::Error::new(io::ErrorKind::Other, err)),
            };

            completed_count += completed;
            self.in_flight.set(self.in_flight.get() - completed);

            for c in completions.iter_mut().take(completed) {
                // libblkio guarantees this is safe
                let c = unsafe { c.assume_init_mut() };

                let rs_ptr = c.user_data as *const RequestState;
                let rs = unsafe { Rc::from_raw(rs_ptr) };

                let ret = if c.ret < 0 { c.ret } else { 0 };
                let old_val = rs.ret.replace(ret);
                assert!(old_val == 1);
            }

            if completed < 64 {
                break;
            }
        }

        if completed_count > 0 {
            if !(self.in_flight.get() == 0 && completed_count == 1) {
                // Notify all requests (which includes the completed ones).  If `in_flight` is 0
                // and `completed_count` is 1, the calling request was the only one in flight, and
                // it has been completed.  No need to wake anything then.
                cx.waker().wake_by_ref();
            }

            #[cfg(debug_assertions)]
            if self.in_flight.get() == 0 {
                // When there are no requests in flight, moving the queue to a different context is
                // safe, so clear the reference waker
                self.compare_waker.take();
            }

            // Clear the completion FD event
            let mut buf = std::mem::MaybeUninit::<u64>::uninit();
            let _ = unsafe {
                libc::read(
                    self.raw_completion_fd,
                    buf.as_mut_ptr() as *mut libc::c_void,
                    std::mem::size_of::<u64>(),
                )
            };
        }

        Ok(())
    }
}

impl RequestState {
    /**
     * Create a new `RequestState` object for the given queue.
     */
    pub fn new(q: &Rc<Queue>, owned_iovec: Option<Box<[libc::iovec]>>) -> Self {
        RequestState {
            ret: Cell::new(1),
            completion_fd: Cell::new(None),
            queue: Rc::clone(q),
            owned_iovec,
        }
    }

    /**
     * Check whether this request has completed, and if so, return the result.
     */
    fn check_completion(&self) -> Option<io::Result<()>> {
        match self.ret.get() {
            0 => Some(Ok(())),
            err if err < 0 => Some(Err(io::Error::from_raw_os_error(-err))),
            _ => None,
        }
    }

    /**
     * Try to await completion FD readability.  Return true if someone waits on it, not necessarily
     * this request.  Return false otherwise, which means we should re-poll completions and try
     * again.
     */
    fn await_completion_fd(&self, cx: &mut Context<'_>) -> io::Result<bool> {
        if let Some(cfd) = self.queue.completion_fd.take() {
            let polled_cfd = cfd.poll_read_ready(cx);
            match polled_cfd {
                // Pending: Take ownership of the completion FD
                Poll::Pending => {
                    self.completion_fd.set(Some(cfd));
                    Ok(true)
                }

                // Ready: Return ownership to the queue, clear readability, and return `false`
                Poll::Ready(Ok(mut g)) => {
                    g.clear_ready();
                    self.queue.completion_fd.set(Some(cfd));
                    Ok(false)
                }

                // Ready with error: Return ownership to the queue, return the error
                Poll::Ready(Err(err)) => {
                    self.queue.completion_fd.set(Some(cfd));
                    Err(err)
                }
            }
        } else {
            // Someone else is waiting on the completion FD
            Ok(true)
        }
    }

    /**
     * Fetch completions from libblkio, and then check this request for completion.  If the request
     * is still pending, have the context be woken by activity on the completion FD.
     */
    fn submit_and_poll(self: &Rc<Self>, cx: &mut Context<'_>) -> Poll<io::Result<()>> {
        if let Err(err) = self.queue.poll_completions(cx) {
            return Poll::Ready(Err(err));
        }

        match self.check_completion() {
            Some(result) => Poll::Ready(result),
            None => match self.await_completion_fd(cx) {
                Ok(true) => Poll::Pending,
                Ok(false) => self.submit_and_poll(cx), // retry
                Err(err) => Poll::Ready(Err(err)),
            },
        }
    }
}

impl Future for Request<'_> {
    type Output = io::Result<()>;

    /**
     * Poll this request for progress.
     */
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        // Verify that all requests are always in the same context (by comparing wakers)
        #[cfg(debug_assertions)]
        {
            let mut compare_waker = self.rs.queue.compare_waker.take();
            if let Some(w) = compare_waker.as_ref() {
                assert!(w.will_wake(cx.waker()));
            } else {
                compare_waker = Some(cx.waker().clone());
            }
            self.rs.queue.compare_waker.set(compare_waker);
        }

        #[cfg(feature = "x-blkio-test")]
        self.rs
            .queue
            .blkioq
            .borrow_mut()
            .notify_poll(Rc::as_ptr(&self.rs) as usize);

        // If we took ownership of and waited for the completion FD, return it to the queue now
        if let Some(cfd) = self.rs.completion_fd.take() {
            self.rs.queue.completion_fd.set(Some(cfd));
        }

        // Quick path: Is this request already completed?
        if let Some(result) = self.rs.check_completion() {
            return Poll::Ready(result);
        }

        // Slow path: Run `do_io()`, fetch completions, potentially poll the completion FD.
        self.rs.submit_and_poll(cx)
    }
}

impl<'a> Drop for Request<'a> {
    fn drop(&mut self) {
        // Ensure ownership of the completion FD is returned to the queue when the owning request
        // is dropped
        if let Some(cfd) = self.rs.completion_fd.take() {
            self.rs.queue.completion_fd.set(Some(cfd));
        }

        // Do not decrease `self.rs.queue.in_flight`.  The `RequestState` will outlive `Request`
        // until the request is actually done, and then `in_flight` will be decremented.  That is
        // intentional.
    }
}
